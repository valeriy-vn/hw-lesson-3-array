let styles = ['Джаз', 'Блюз'];

// Вставляем в конец массива
styles.push('Рок-н-Ролл');
console.log('Array with Rock: ', styles);

// Находим середину массива
let middle = Math.floor(styles.length / 2);
styles[middle] = 'Классика';
console.log('A new array is: ', styles);

// Удалить 1-й элемент массива

let removed = styles.splice(0,1);
console.log('1-st element of Array was removed: ', removed);

// Вставить в начале массива Рэп и Регги

styles.unshift("Рэп", "Регги" )
console.log('Final Array is: ', styles);
